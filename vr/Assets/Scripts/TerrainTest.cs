﻿using UnityEngine;
using System.Collections;

public class TerrainTest : MonoBehaviour {
    private Mesh mMesh;
    Vector3[] Vertices;
	// Use this for initialization
	void Start () {
        mMesh = GetComponent<MeshFilter>().mesh;
        Vertices = mMesh.vertices;
        GenerateNormals();
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.A))
        {
            for (int i = 0; i < Vertices.Length; i++)
            {
                if(Vertices[i].y < 10.0f)
                {
                    Vertices[i].y += Random.Range(0.0f, 15.0f);
                }
            }
        }
        mMesh.vertices = Vertices;
	}
    void GenerateNormals()
    {

    }
    void Cross(Vector3 a, Vector3 b)
    {

    }
}
