﻿using UnityEngine;
using System.Collections;

public class MechController : MonoBehaviour {
	public Transform cam;

	public float moveSpeed = 1;
	public float turnSpeed = 1;
	public float boostUp = 10;
	public float maximumSpeed = 100;

	Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		
	}
	
	// Update is called once per frame
	void Update () {
		Debug.DrawRay(transform.position, transform.forward);

		if (Input.GetButton("Reset"))
		{
			UnityEngine.VR.InputTracking.Recenter();
		}

		if (Input.GetButtonDown("BoostUp"))
		{
			rb.AddForce(transform.up * boostUp);
		}
		float turn = Input.GetAxis("Horizontal");
		transform.Rotate(Vector3.up, turn * turnSpeed);
		//rb.angularVelocity = new Vector3(0, turn * turnSpeed, 0);
	}

	void FixedUpdate()
	{
		//float turn = Input.GetAxis("Horizontal");
		//rb.AddTorque(0, turn * turnSpeed, 0);
		float fwd = Input.GetAxis("Vertical");
		if (Mathf.Abs(fwd) > .0f)
		{
			//rb.velocity = transform.forward * moveSpeed * fwd;
			rb.AddForce(transform.forward * moveSpeed * fwd);
		}
		if (Input.GetButton("BoostUp"))
		{
			rb.AddForce(transform.up * boostUp*.05f);
		}
		Vector3 velXZ = new Vector3(rb.velocity.x, 0, rb.velocity.z);
		float sqSpeed = velXZ.sqrMagnitude;
		if (sqSpeed > maximumSpeed * maximumSpeed)

		{
			float brakeSpeed = Mathf.Sqrt(sqSpeed) - maximumSpeed;  // calculate the speed decrease

			Vector3 normalisedVelocity = velXZ.normalized;
			Vector3 brakeVelocity = normalisedVelocity * brakeSpeed;  // make the brake Vector3 value

			rb.AddForce(-brakeVelocity);  // apply opposing brake force
		}
	}
}
