﻿using UnityEngine;
using System.Collections;

public class DelayedFollow : MonoBehaviour {

	public Transform targetPos;
	public Transform cam;
	public float speed = 1;
	public float rotSpeed = 360;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void LateUpdate()
	{
		//transform.position = Vector3.MoveTowards(transform.position, targetPos.position, speed * Time.deltaTime);
		transform.position = cam.position;
		transform.rotation = Quaternion.Slerp(transform.rotation, cam.transform.rotation, rotSpeed);
		//transform.Rotate()
	}
}
