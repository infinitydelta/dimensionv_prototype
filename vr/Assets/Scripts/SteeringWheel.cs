﻿using UnityEngine;
using System.Collections;

public class SteeringWheel : MonoBehaviour {

	Quaternion left, right;
	// Use this for initialization
	void Start () {
		left = Quaternion.Euler(0, 0, 110);
		right = Quaternion.Euler(0, 0, -110);
	}
	
	// Update is called once per frame
	void Update () {
		float h = Input.GetAxis("Horizontal");
		transform.localRotation = Quaternion.Slerp(left, right, (h+1)/2);
	}
}
