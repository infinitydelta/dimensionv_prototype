﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	public enum axis { x, y, z}
	// Use this for initialization
	public axis rotationAxis;
	public Vector3 axisOfRotation = Vector3.up;
	public float speed = 20;

	Transform thisTransform;
	void Start () {
		thisTransform = this.transform;
	}
	
	// Update is called once per frame
	void Update () {
		thisTransform.Rotate(axisOfRotation, speed * Time.deltaTime);

		//if (rotationAxis == axis.x)
		//{
		//}
	}
}
