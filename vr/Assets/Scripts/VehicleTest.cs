﻿using UnityEngine;
using System.Collections;

public class VehicleTest : MonoBehaviour {

	public float steeringAngle = 40;
	public float accelForce = 10;
	public float maxRotationDelta = .01f;
	public float rbSteeringValue = .01f;
	public float turnStrength = 10;
	public float turnStrengthSpeedConst = .1f;
	public float hoverForce = 9;
	public float hoverHeight = 2;

	public Transform[] hoverPoints;


	Vector3 steeringVectorLeft, steeringVectorRight;
	Vector3 steeringVector;

	Transform thisTransform = null;
	Rigidbody rb = null;

	int hoverLayerMask;


	// Use this for initialization
	void Start () {
		thisTransform = GetComponent<Transform>();
		rb = GetComponent<Rigidbody>();

		UnityEngine.VR.InputTracking.Recenter();

		hoverLayerMask = 1 << LayerMask.NameToLayer("vehicle");
		hoverLayerMask = ~hoverLayerMask;

	}

	// Update is called once per frame
	void Update () {

		Debug.DrawRay(thisTransform.position, steeringVectorLeft, Color.red);
		Debug.DrawRay(thisTransform.position, steeringVectorRight, Color.red);

		if (Input.GetButtonDown("Reset") || Input.GetKeyDown("r"))
		{
			print("r pressed");
			UnityEngine.VR.InputTracking.Recenter();
		}


		//steering
		steeringVectorLeft = Quaternion.AngleAxis(-steeringAngle, thisTransform.up) * thisTransform.forward;
		steeringVectorRight = Quaternion.AngleAxis(steeringAngle, thisTransform.up) * thisTransform.forward;
		float t = (Input.GetAxis("Horizontal") + 1) / 2;
		steeringVector = Vector3.Lerp(steeringVectorLeft, steeringVectorRight, t);
		Debug.DrawRay(thisTransform.position, steeringVector, Color.cyan);




	}

	void FixedUpdate()
	{
		hover2();

		if (Input.GetAxisRaw("Accelerate") > 0)
		{
			//rb.AddForce((thisTransform.forward + steeringVector).normalized * accelForce, ForceMode.Acceleration);
			rb.AddForce(thisTransform.forward * accelForce, ForceMode.Acceleration);
			Debug.DrawRay(thisTransform.position, (thisTransform.forward + steeringVector).normalized * accelForce, Color.white);

		}
		//if (Input.GetAxisRaw("Brake") > 0)
		//{
		//	rb.velocity *= .95f;
		//}
		rb.AddRelativeTorque(Vector3.up * Input.GetAxis("Horizontal") * (turnStrength + rb.velocity.sqrMagnitude * turnStrengthSpeedConst), ForceMode.Acceleration);

		//if (rb.velocity.sqrMagnitude > 0.005f * 0.005f)
		//{
		//	thisTransform.forward = Vector3.RotateTowards(thisTransform.forward, steeringVector, maxRotationDelta, 0);
		//	rb.velocity = Vector3.RotateTowards(rb.velocity, thisTransform.forward, rbSteeringValue, 0);
		//}
		rb.velocity = Vector3.ClampMagnitude(rb.velocity, 200);

		stabilize();

	}

	void hover()
	{
		RaycastHit hit;
		for (int i = 0; i < hoverPoints.Length; i++)
		{
			Transform hp = hoverPoints[i];
			Debug.DrawRay(hp.position, Vector3.down);
			//hit something
			if (Physics.Raycast(hp.position, Vector3.down, out hit, hoverHeight, hoverLayerMask))
			{
				Debug.DrawRay(hp.position, Vector3.down * hit.distance, Color.green);


				//if (hit.distance < .25f)
				//{
				//	rb.AddForceAtPosition(Vector3.up * hoverForce * (1 - hit.distance / hoverHeight), hp.position, ForceMode.Acceleration);

				//}
				//else
				//{
				//	rb.AddForceAtPosition(Vector3.up * hoverForce * .1f, hp.position, ForceMode.Acceleration);

				//}
				rb.AddForceAtPosition(Vector3.up * hoverForce * (1 - (hit.distance / hoverHeight)), hp.position, ForceMode.Force);
			}
			else //hoverpoint not hitting anything, upside down or on ledge
			{
				Debug.DrawRay(hp.position, Vector3.down * hoverHeight, Color.red);

				//if (thisTransform.position.y > hp.position.y)
				//{
				//	rb.AddForceAtPosition(hp.up * hoverForce, hp.position, ForceMode.Force);
				//}
				//else
				//{
				//	rb.AddForceAtPosition(hp.up * -hoverForce, hp.position, ForceMode.Force);
				//}
			}
		}
	}

	void hover2()
	{
		RaycastHit hit;
		for (int i = 0; i < hoverPoints.Length; i++)
		{
			Transform hp = hoverPoints[i];
			Debug.DrawRay(hp.position, Vector3.down);
			//hit something
			if (Physics.Raycast(hp.position, Vector3.down, out hit, hoverHeight, hoverLayerMask))
			{
				Debug.DrawRay(hp.position, Vector3.down * hit.distance, Color.green);

				rb.AddForce(Vector3.up * hoverForce * (1 - (hit.distance / hoverHeight)), ForceMode.Acceleration);
			}
			//else //hoverpoint not hitting anything, upside down or on ledge
			//{
			//	Debug.DrawRay(hp.position, Vector3.down * hoverHeight, Color.red);

			//	//if (thisTransform.position.y > hp.position.y)
			//	//{
			//	//	rb.AddForceAtPosition(hp.up * hoverForce, hp.position, ForceMode.Force);
			//	//}
			//	//else
			//	//{
			//	//	rb.AddForceAtPosition(hp.up * -hoverForce, hp.position, ForceMode.Force);
			//	//}
			//}
		}
	}

	private void stabilize()
	{
		//rb.rotation.eulerAngles.Set(rb.rotation.eulerAngles.x, rb.rotation.eulerAngles.y, 0);
		thisTransform.localEulerAngles.Set(thisTransform.localRotation.x, thisTransform.localRotation.y, 0);
	}

}
