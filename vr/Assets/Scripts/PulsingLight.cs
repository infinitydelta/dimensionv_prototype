﻿using UnityEngine;
using System.Collections;

public class PulsingLight : MonoBehaviour {
	public float speed = 1;
	public float range = 20;
	public float minSize = 0;
	Light light;

	// Use this for initialization
	void Start () {
		light = GetComponent<Light>();
		range = light.intensity;
	}
	
	// Update is called once per frame
	void Update () {
		light.intensity = range * (Mathf.Cos(Time.time * speed) + 1)*.5f + minSize;
	}
}
