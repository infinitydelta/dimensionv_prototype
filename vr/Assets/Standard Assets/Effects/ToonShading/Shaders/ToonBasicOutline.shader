Shader "Toon/Basic Outline" {
	Properties{
		_Color("Main Color", Color) = (.5,.5,.5,1)
		_OutlineColor("Outline Color", Color) = (0,0,0,1)
		_Outline("Outline width", Range(-1, 10)) = .005
		_xmagnitude("x strength", Float) = 1
		_ymagnitude("y strength", Float) = 1
		_zmagnitude("z strength", Float) = 1

		_MainTex("Base (RGB)", 2D) = "white" { }
	_ToonShade("ToonShader Cubemap(RGB)", CUBE) = "" { }
	_Noise("Noise", 2D) = "" {}
	}

		CGINCLUDE
#include "UnityCG.cginc"

	struct appdata {
		float4 vertex : POSITION;
		float3 normal : NORMAL;
	};

	struct v2f {
		float4 pos : SV_POSITION;
		UNITY_FOG_COORDS(0)
			fixed4 color : COLOR;
	};

	uniform float _Outline;
	uniform float4 _OutlineColor;
	uniform float _xmagnitude;
	uniform float _ymagnitude;
	uniform float _zmagnitude;
	sampler2D _Noise;

	v2f vert(appdata v) {
		v2f o;
		//v.vertex + float4(0, 10, 0, 0);
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex );

		float xMag = tex2Dlod(_Noise, float4(v.vertex.x, v.vertex.y, v.vertex.z, 1.0f) * _SinTime.x).r;
		float yMag = tex2Dlod(_Noise, float4(v.vertex.x, v.vertex.y, v.vertex.z, 1.0f) * _SinTime.x).g;
		float zMag = tex2Dlod(_Noise, float4(v.vertex.x, v.vertex.y, v.vertex.z, 1.0f) * _SinTime.x).b;
		float3 worldPos = mul(_Object2World, v.vertex).xyz;
		float distanceToCamera = distance(_WorldSpaceCameraPos, worldPos);
		float3 norm = normalize(mul((float3x3)UNITY_MATRIX_IT_MV, v.normal));
		float2 offset = TransformViewToProjection(norm.xy);
		//o.pos.x += xMag * _Outline;
		//o.pos.y += yMag * _Outline;
		//o.pos.z += zMag * _Outline;
		//float op = o.pos * float3(_xmagnitude, _ymagnitude, _zmagnitude);
		//float op = _xmagnitude;
		//o.pos.xy += offset  * _Outline * clamp(distanceToCamera - 50, 0, 99999) * xMag;
		o.color = _OutlineColor;
		UNITY_TRANSFER_FOG(o, o.pos);
		return o;
	}
	ENDCG

		SubShader{
		Tags{ "RenderType" = "Opaque" }
		//UsePass "Toon/Basic/BASE"
		//UsePass "Standard"
		Pass{
		Name "OUTLINE"
		Tags{ "LightMode" = "Always" }
		Cull Off
		ZWrite On
		ColorMask RGB
		Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma multi_compile_fog
		fixed4 frag(v2f i) : SV_Target
	{
		UNITY_APPLY_FOG(i.fogCoord, i.color);
	return i.color;
	}
		ENDCG
	}
	}

		Fallback "Toon/Basic"
}