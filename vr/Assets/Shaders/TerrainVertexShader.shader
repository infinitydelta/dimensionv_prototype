﻿Shader "Custom/TerrainVertexShader"
{
	Properties
	{
		_MainTex ("Noise Texture", 2D) = "white" {}
		_SampleConst ("Sampling Const", Float) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				//float2 uv : TEXCOORD0;
				float4 color : COLOR;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _SampleConst;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				float worldheight = mul(_Object2World, v.vertex).y;
				float f = smoothstep(150, -50, worldheight);
				float4 col = lerp(float4(1, 0, 0, 1), float4(0, 0, 0, 1), f);
				o.color = col;
				//o.color = tex2Dlod(_MainTex, (abs(v.vertex)* _SampleConst)%1);
				//o.color = v.color;
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				//return col;
				return i.color;
			}
			ENDCG
		}
	}
}
