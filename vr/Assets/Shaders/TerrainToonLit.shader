﻿Shader "Custom/TerrainToonLit" {
	Properties{
		_Color("Main Color", Color) = (0.5,0.5,0.5,1)
		_Color2("Second Color", Color) = (0,0,0,1)
		_HeightUpper("Height upper limit", Float) = 100
		_HeightLower("Height lower limit", Float) = 0
		_MinimumBrightness("Minimum Brightness", Range(0,1)) = 0
		_Darkness("Darkness", Range(0,.75)) = 0
		_FragmentationValue("Fragmentation Value", Range(0,100)) = 0
		_FragmentationMinDistance("Fragmentation Min Distance", Range(0,100)) = 10
		_FragmentationMaxDistance("Fragmentation Max Distance", Range(0,1000)) = 10
		_MainTex("Base (RGB)", 2D) = "white" {}
		_Ramp("Toon Ramp (RGB)", 2D) = "white" {}
		_Noise("Noise", 2D) = "white" {}
	}

		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200
		


		CGPROGRAM
#pragma surface surf ToonRamp vertex:vert 

		sampler2D _Ramp;
		float _Darkness;


	// custom lighting function that uses a texture ramp based
	// on angle between light direction and normal
//#pragma target 3.0
#pragma lighting ToonRamp exclude_path:prepass 
	inline half4 LightingToonRamp(SurfaceOutput s, half3 lightDir, half atten)
	{
#ifndef USING_DIRECTIONAL_LIGHT
		lightDir = normalize(lightDir);
#endif

		half d = dot(s.Normal, lightDir)*0.5 + 0.5;
		//half bot = .3f;
		//if (d > bot) {
		//	d = fmod(d + _Time.x, bot) + bot;
		//}
		//half3 ramp = tex2D(_Ramp, max(float2(.0f,.0f), fmod(float2(d,d) + _Darkness, 1))).rgb;
		half3 ramp = tex2D(_Ramp, (float2(d, d) )).rgb;

		half4 c;
		c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
		c.a = 0;
		return c;
	}


	sampler2D _MainTex;
	sampler2D _Noise;
	float4 _Color;
	float4 _Color2;
	float _HeightUpper;
	float _HeightLower;
	float _MinimumBrightness;

	float _FragmentationValue;
	float _FragmentationMinDistance;
	float _FragmentationMaxDistance;
	
	struct Input {
		float4 vertex : POSITION;
		float2 uv_MainTex : TEXCOORD0;
		//float3 worldPos;
		//float3 normal : NORMAL;
		float4 color : COLOR;
	};

	float rand(float3 co)
	{
		return frac(sin(dot(co.xyz, float3(12.9898, 78.233, 45.5432))) * 43758.5453);
	}

	void vert(inout appdata_full v) {
		float3 worldPos = mul(_Object2World, v.vertex);
		

		float f = smoothstep(_HeightUpper, _HeightLower, worldPos.y);
		//float4 nearblack = float4(0.1f,0.1f,0.1f, 1);
		//float f_loop = fmod(f + _Time.x, 1);
		v.color = lerp(_Color, _Color2, f);
		
		float3 randTex = tex2Dlod(_Noise, float4(worldPos.x, worldPos.z, 0, 1)).rgb;
		float random_number = rand((worldPos));
		float distanceToCamera = distance(_WorldSpaceCameraPos, worldPos);
		float fragmentationPercentage = clamp(distanceToCamera - _FragmentationMinDistance, 0, _FragmentationMaxDistance) / (_FragmentationMaxDistance - _FragmentationMinDistance);
		
		float3 worldNormal = mul(_Object2World,v.normal);
		float3 d = normalize(worldPos - _WorldSpaceCameraPos);
		worldPos += (d+float3(0,1,0)) * _FragmentationValue;
		//worldPos += (randTex * fragmentationPercentage *  _FragmentationValue);
		//v.vertex.xyz += (randTex  * fragmentationPercentage *  _FragmentationValue);
		v.vertex.xyz = mul(_World2Object, float4(worldPos, 1));
		
	}


	void surf(Input IN, inout SurfaceOutput o) {
		half4 c = (tex2D(_MainTex, IN.uv_MainTex)+half4(_MinimumBrightness, _MinimumBrightness, _MinimumBrightness, _MinimumBrightness)) * IN.color;
		//half4 c = half4(1, 0, 0, 1);
		
		o.Albedo = c.rgb;
		//o.Gloss = 1;
		//o.Albedo = IN.color;
		o.Alpha = c.a;
	}
	ENDCG

	}

		Fallback "Diffuse"
}
