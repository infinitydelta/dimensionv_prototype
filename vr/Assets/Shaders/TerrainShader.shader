﻿Shader "Custom/TerrainShader" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_FragmentationValue("Fragmentation Value", Range(0,10)) = 0
		_FragmentationMinDistance("Fragmentation Min Distance", Range(0,100)) = 10
		_FragmentationMaxDistance("Fragmentation Max Distance", Range(0,1000)) = 10
		_Noise("Noise", 2D) = "" {}
	}
		SubShader{
		Tags{ "RenderType" = "Transparent" }
		LOD 200
		Cull Back
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard vertex:vert fullforwardshadows alpha:fade

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;

	struct Input {
		float2 uv_MainTex;
		float3 worldPos;
	};

	float _FragmentationValue;
	float _FragmentationMinDistance;
	float _FragmentationMaxDistance;

	void vert(inout appdata_full v) {
		float3 worldPos = mul(_Object2World, v.vertex).xyz;
		float distanceToCamera = distance(_WorldSpaceCameraPos, worldPos);
		float fragmentationPercentage = clamp(distanceToCamera - _FragmentationMinDistance,0,_FragmentationMaxDistance) / (_FragmentationMaxDistance - _FragmentationMinDistance);
		v.vertex.xyz += v.normal * fragmentationPercentage *  _FragmentationValue;
		//v.color = float4(1, 0, 0, 1);
	}

	half _Glossiness;
	half _Metallic;
	fixed4 _Color;

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		o.Albedo = c.rgb;
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		//o.Alpha = c.a;
		fixed distanceToCamera = distance(_WorldSpaceCameraPos, IN.worldPos);
		fixed fragmentationPercentage = -(distanceToCamera - _FragmentationMaxDistance) / (_FragmentationMaxDistance - _FragmentationMinDistance);
		o.Alpha = fragmentationPercentage;
	}
	ENDCG
	}
		FallBack "Diffuse"
}